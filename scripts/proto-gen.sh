#!/bin/bash

protoc -I /usr/local/include \
    -I . \
    -I $GOPATH/src \
    -I $GOPATH/src/github.com/grpc-ecosystem/grpc-gateway/third_party/googleapis \
    -I $GOPATH/src/github.com/gogo/protobuf/protobuf \
    --proto_path=api/proto/v1 \
    --gofast_out=plugins=grpc:pkg/api/v1 \
    mail-service.proto