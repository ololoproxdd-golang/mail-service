package health

import (
	"context"
	v1 "gitlab.com/ololoproxdd-golang/mail-service/pkg/api/v1"
)

type HealthCheck interface {
	Ping(context.Context) (*v1.PingResponse, error)
	Health(context.Context) (*v1.HealthResponse, error)
}
