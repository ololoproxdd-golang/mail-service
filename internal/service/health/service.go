package health

import (
	"context"
	v1 "gitlab.com/ololoproxdd-golang/mail-service/pkg/api/v1"
)

type health struct {
}

func NewHealthService() *health {
	s := &health{}
	return s
}

func (h health) Ping(context.Context) (*v1.PingResponse, error) {
	return &v1.PingResponse{Message: "Pong"}, nil
}

func (h health) Health(context.Context) (*v1.HealthResponse, error) {
	return &v1.HealthResponse{Message: "Status: Ok"}, nil
}
