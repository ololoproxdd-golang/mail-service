package mail

import (
	"context"
	v1 "gitlab.com/ololoproxdd-golang/mail-service/pkg/api/v1"
)

type MailService interface {
	Send(context.Context, v1.SendRequest) (*v1.SendResponse, error)
}
