package mail

import (
	"context"
	"github.com/go-gomail/gomail"
	"github.com/pkg/errors"
	"gitlab.com/ololoproxdd-golang/mail-service/internal/config"
	v1 "gitlab.com/ololoproxdd-golang/mail-service/pkg/api/v1"
)

type mailService struct {
	cfg config.Config
}

func NewMailService(cfg config.Config) *mailService {
	s := &mailService{
		cfg,
	}

	return s
}

func (s *mailService) Send(ctx context.Context, req v1.SendRequest) (*v1.SendResponse, error) {
	m := gomail.NewMessage()
	m.SetHeader("From", s.cfg.EmailAddress)
	m.SetHeader("To", req.To)
	m.SetHeader("Subject", req.Subject)
	m.SetBody(req.ContentType, req.Body)

	d := gomail.NewDialer(s.cfg.SmtpHost, s.cfg.SmtpPort, s.cfg.EmailAddress, s.cfg.EmailPassword)

	if err := d.DialAndSend(m); err != nil {
		return nil, errors.Wrap(err, "Send email error.")
	}

	return &v1.SendResponse{
		Message: "Send email success",
	}, nil
}
