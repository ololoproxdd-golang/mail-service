package server

import (
	"context"
	"fmt"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"google.golang.org/grpc"
	"io"
	"os"
	"time"
)

// init logger
func (s *server) initLogger() {
	zerolog.MessageFieldName = "MESSAGE"
	zerolog.LevelFieldName = "LEVEL"

	zerolog.SetGlobalLevel(s.getLoggerLevel())
	log.Logger = log.Output(s.getLoggerWriter()).
		With().Str("PROGRAM", "mail-service").
		Str("HOST_NAME", s.config.HostName).
		Logger()
}

func (s *server) getLoggerWriter() io.Writer {
	var writer io.Writer
	writer = os.Stdout

	if s.config.UseSyslog() {
		var err error
		writer, err = os.OpenFile(s.config.LogPath, os.O_WRONLY|os.O_APPEND, os.FileMode(0666))
		if err != nil {
			log.Warn().Err(err).Msg("cannot open logs file")
			writer = os.Stdout
		} else if !s.config.IsProd() {
			writer = io.MultiWriter(writer, os.Stdout)
		}
	}
	return writer
}

func (s *server) getLoggerLevel() zerolog.Level {
	if s.config.IsProd() {
		return zerolog.InfoLevel
	}

	return zerolog.DebugLevel
}

func GrpcLoggerInterceptor(
	ctx context.Context, req interface{}, info *grpc.UnaryServerInfo, handler grpc.UnaryHandler,
) (interface{}, error) {

	start := time.Now()
	h, err := handler(ctx, req)
	duration := time.Since(start)
	errText := ""
	if err != nil {
		errText = err.Error()
	}

	log.Info().
		Str("Method", info.FullMethod).
		Str("Error", errText).
		Msg(
			fmt.Sprintf(
				"Duration:%s\tError:%v",
				duration,
				err,
			),
		)

	return h, err
}
