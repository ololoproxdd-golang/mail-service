package server

import (
	"github.com/rs/zerolog/log"
	"gitlab.com/ololoproxdd-golang/mail-service/internal/service/health"
	"gitlab.com/ololoproxdd-golang/mail-service/internal/service/mail"
	"gitlab.com/ololoproxdd-golang/mail-service/internal/transport"
	v1 "gitlab.com/ololoproxdd-golang/mail-service/pkg/api/v1"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

func (s *server) getMailService() mail.MailService {
	if s.mailService != nil {
		return s.mailService
	}
	s.mailService = mail.NewMailService(s.config)
	log.Info().Msgf("created mail service")

	return s.mailService
}

func (s *server) getHealthService() health.HealthCheck {
	if s.healthService != nil {
		return s.healthService
	}
	s.healthService = health.NewHealthService()
	log.Info().Msgf("created health service")

	return s.healthService
}

func (s *server) getEndpoints() *transport.MailEndpoints {
	if s.endpoints != nil {
		return s.endpoints
	}
	s.endpoints = transport.NewEndpoints(s.getMailService(), s.getHealthService())
	log.Info().Msgf("created server endpoints")
	return s.endpoints

}

func (s *server) getTransportGrpc() *grpc.Server {
	if s.grpcServer != nil {
		return s.grpcServer
	}
	s.grpcServer = grpc.NewServer(grpc.UnaryInterceptor(GrpcLoggerInterceptor))
	v1.RegisterMailServiceServer(s.grpcServer, transport.MakeGrpcServer(s.getEndpoints()))

	// Register reflection service on gRPC server.
	reflection.Register(s.grpcServer)

	log.Info().Msgf("created transport - grpc")

	return s.grpcServer
}
