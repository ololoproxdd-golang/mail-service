package server

import (
	"github.com/rs/zerolog/log"
	"gitlab.com/ololoproxdd-golang/mail-service/internal/config"
	"gitlab.com/ololoproxdd-golang/mail-service/internal/service/health"
	"gitlab.com/ololoproxdd-golang/mail-service/internal/service/mail"
	"gitlab.com/ololoproxdd-golang/mail-service/internal/transport"
	"google.golang.org/grpc"
	"sync"
)

type server struct {
	grpcServer *grpc.Server

	config        config.Config
	endpoints     *transport.MailEndpoints
	mailService   mail.MailService
	healthService health.HealthCheck
}

func NewServer(config config.Config) *server {
	return &server{config: config}
}

// close all descriptors
func (s *server) Close() {
	s.grpcServer.Stop()
}

// open init structure
func (s *server) Open() error {
	s.initLogger()
	s.registerOsSignal()

	return nil
}

func (s *server) Run() {
	wg := sync.WaitGroup{}

	wg.Add(1)
	go s.runGrpcServer(&wg)

	wg.Wait()
	log.Info().Msg("mail-service stopped...")
}
