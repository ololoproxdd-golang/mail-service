package transport

import (
	"context"
	kitgrpc "github.com/go-kit/kit/transport/grpc"
	"github.com/golang/protobuf/ptypes/empty"
	v1 "gitlab.com/ololoproxdd-golang/mail-service/pkg/api/v1"
)

type GRPCServer struct {
	ping   kitgrpc.Handler
	health kitgrpc.Handler
	send   kitgrpc.Handler
}

func MakeGrpcServer(endpoints *MailEndpoints) v1.MailServiceServer {
	opts := []kitgrpc.ServerOption{
		kitgrpc.ServerErrorHandler(NewErrorHandler("grpc")),
	}

	return &GRPCServer{
		ping: kitgrpc.NewServer(
			endpoints.Ping,
			noEncodeDecode,
			noEncodeDecode,
			opts...),
		health: kitgrpc.NewServer(
			endpoints.Health,
			noEncodeDecode,
			noEncodeDecode,
			opts...),
		send: kitgrpc.NewServer(
			endpoints.Send,
			noEncodeDecode,
			noEncodeDecode,
			opts...),
	}
}

// function not encoding
func noEncodeDecode(_ context.Context, r interface{}) (interface{}, error) {
	return r, nil
}

func (s *GRPCServer) Ping(ctx context.Context, req *empty.Empty) (*v1.PingResponse, error) {
	_, resp, err := s.ping.ServeGRPC(ctx, *req)
	if err != nil {
		return nil, err
	}

	return resp.(*v1.PingResponse), nil
}

func (s *GRPCServer) Health(ctx context.Context, req *empty.Empty) (*v1.HealthResponse, error) {
	_, resp, err := s.health.ServeGRPC(ctx, *req)
	if err != nil {
		return nil, err
	}

	return resp.(*v1.HealthResponse), nil
}

func (s *GRPCServer) Send(ctx context.Context, req *v1.SendRequest) (*v1.SendResponse, error) {
	_, resp, err := s.send.ServeGRPC(ctx, *req)
	if err != nil {
		return nil, err
	}

	return resp.(*v1.SendResponse), nil
}
