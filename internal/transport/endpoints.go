package transport

import (
	"context"
	"github.com/go-kit/kit/endpoint"
	"github.com/rs/zerolog/log"
	"gitlab.com/ololoproxdd-golang/mail-service/internal/service/health"
	"gitlab.com/ololoproxdd-golang/mail-service/internal/service/mail"
	v1 "gitlab.com/ololoproxdd-golang/mail-service/pkg/api/v1"
)

type MailEndpoints struct {
	Send   endpoint.Endpoint
	Ping   endpoint.Endpoint
	Health endpoint.Endpoint
}

func NewEndpoints(s mail.MailService, c health.HealthCheck) *MailEndpoints {
	e := &MailEndpoints{
		Send:   makeSendEndpoint(s),
		Ping:   makePingEndpoint(c),
		Health: makeHealthEndpoint(c),
	}
	return e
}

func makeSendEndpoint(s mail.MailService) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (response interface{}, err error) {
		req, ok := request.(v1.SendRequest)
		if !ok {
			log.Fatal().Msg("makeSendEndpoint not type of v1.SendRequest")
		}

		return s.Send(ctx, req)
	}
}

func makePingEndpoint(s health.HealthCheck) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (response interface{}, err error) {
		return s.Ping(ctx)
	}
}

func makeHealthEndpoint(s health.HealthCheck) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (response interface{}, err error) {
		return s.Health(ctx)
	}
}
